﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taviloglu.Wrike.Core.Tasks;
using WrikeConnectorModule.Models;

namespace WrikeConnectorModule.Services
{
    public class ElasticAgent : ElasticBaseAgent
    {


        public async Task<ResultItem<List<clsObjectRel>>> GetIssueRelations(List<WrikeTask> tasks)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var searchIssuesIds = tasks.Select(issue => new clsOntologyItem
                {
                    Name = issue.Id,
                    GUID_Parent = Config.LocalData.Class_Wrike_Id.GUID
                }).ToList();

                if (!searchIssuesIds.Any()) return result;

                var dbReaderIssuesIds = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderIssuesIds.GetDataObjects(searchIssuesIds);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the issue-Ids!";
                    return result;
                }

                var searchIssues = dbReaderIssuesIds.Objects1.Select(id => new clsObjectRel
                {
                    ID_Parent_Object = Config.LocalData.Class_Issue__Jira_.GUID,
                    ID_RelationType = Config.LocalData.RelationType_is_defined_by.GUID,
                    ID_Other = id.GUID
                }).ToList();

                if (!searchIssues.Any()) return result;
                var dbReaderIssues = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderIssues.GetDataObjectRel(searchIssues);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting relations between issue-Ids and issues!";
                    return result;
                }

                result.Result = dbReaderIssues.ObjectRels;
                return result;
            });

            return taskResult;
        }
        public async Task<ResultItem<GetModelResult>> GetModel(IGetModelSync request)
        {
            var taskResult = await Task.Run<ResultItem<GetModelResult>>(() =>
            {
                var result = new ResultItem<GetModelResult>()
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetModelResult()
                };

                if (string.IsNullOrEmpty(request.IdConfig) || !globals.is_GUID(request.IdConfig))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No valid Config-Id provided!";
                    return result;
                }

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfigs = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigs.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while loading Configs!";
                    return result;
                }

                result.Result.Config = dbReaderConfigs.Objects1.FirstOrDefault();

                if (result.Result.Config == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Config found!";
                    return result;
                }

                var searchToken = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_WrikeConnectorModule_uses_API_Token.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_WrikeConnectorModule_uses_API_Token.ID_Class_Right
                    }
                };

                var dbReaderConfigToToken = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigToToken.GetDataObjectRel(searchToken);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while loading Authenticationtoken!";
                    return result;
                }

                result.Result.AuthenticationToken = dbReaderConfigToToken.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = globals.Type_Object
                }).FirstOrDefault();

                if (result.Result.AuthenticationToken == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No AuthenticationToken found!";
                    return result;
                }

                var searchTokenKey = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_AttributeType = Config.LocalData.AttributeType_APIToken.GUID,
                        ID_Object = result.Result.AuthenticationToken.GUID
                    }
                };

                var dbReaderTokenKey = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTokenKey.GetDataObjectAtt(searchTokenKey);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while loading Authenticationtoken!";
                    return result;
                }

                result.Result.AuthenticationTokenKey = dbReaderTokenKey.ObjAtts.FirstOrDefault();

                if (result.Result.AuthenticationTokenKey == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Token-Key found!";
                    return result;
                }

                var searchPartner = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = request.IdConfig,
                        ID_RelationType = Config.LocalData.ClassRel_WrikeConnectorModule_belonging_Partner.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_WrikeConnectorModule_belonging_Partner.ID_Class_Right
                    }
                };

                var dbReaderPartner = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderPartner.GetDataObjectRel(searchPartner);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while loading Partner!";
                    return result;
                }

                result.Result.Partner = dbReaderPartner.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = globals.Type_Object
                }).FirstOrDefault();

                if (result.Result.Partner == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Partner found!";
                    return result;
                }

                var searchIssues = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = result.Result.Partner.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Issue__Jira__responsible_for_Partner.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_Issue__Jira__responsible_for_Partner.ID_Class_Left
                    }
                };

                var dbReaderIssuesToPartner = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderIssuesToPartner.GetDataObjectRel(searchIssues);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while loading Issues for Partner!";
                    return result;
                }

                result.Result.IssuesToPartner = dbReaderIssuesToPartner.ObjectRels;

                var searchIssuesToWrikeId = result.Result.IssuesToPartner.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Issue__Jira__is_defined_by_Wrike_Id.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Issue__Jira__is_defined_by_Wrike_Id.ID_Class_Right
                }).ToList();

                if (searchIssuesToWrikeId.Any())
                {
                    var dbReaderIssuesToWrikeId = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderIssuesToWrikeId.GetDataObjectRel(searchIssuesToWrikeId);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Issues to WrikeId!";
                        return result;
                    }

                    result.Result.IssuesToWrikeId = dbReaderIssuesToWrikeId.ObjectRels;
                }

                var searchIssueUrl = result.Result.IssuesToPartner.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Issue__Jira__accessible_by_Url.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Issue__Jira__accessible_by_Url.ID_Class_Right
                }).ToList();

                var dbReaderIssueUrl = new OntologyModDBConnector(globals);

                if (searchIssueUrl.Any())
                {
                    result.ResultState = dbReaderIssueUrl.GetDataObjectRel(searchIssueUrl);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Urls!";
                        return result;
                    }

                    result.Result.IssueUrls = dbReaderIssueUrl.ObjectRels;

                }

                var searchUser = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_WrikeConnectorModule_needs_user.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_WrikeConnectorModule_needs_user.ID_Class_Right

                    }
                };

                var dbReaderUser = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderUser.GetDataObjectRel(searchUser);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the User";
                    return result;
                }

                result.Result.UserItem = dbReaderUser.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.UserItem == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the User!";
                    return result;
                }

                var searchGroup = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_WrikeConnectorModule_needs_Group.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_WrikeConnectorModule_needs_Group.ID_Class_Right

                    }
                };

                var dbReaderGroup = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderGroup.GetDataObjectRel(searchGroup);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Group";
                    return result;
                }

                result.Result.GroupItem = dbReaderGroup.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result.GroupItem == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the Group!";
                    return result;
                }

                var searchSyncTimeEntries = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_AttributeType = Config.LocalData.ClassAtt_WrikeConnectorModule_Sync_Time_Entries.ID_AttributeType
                    }
                };

                var dbReaderSyncTimeEntries = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSyncTimeEntries.GetDataObjectAtt(searchSyncTimeEntries);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error While getting the Sync Time Entries Attribute!";
                    return result;
                }

                result.Result.SyncTimeEntries = dbReaderSyncTimeEntries.ObjAtts.FirstOrDefault();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckObjects(List<clsOntologyItem> checkObjects)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };



                var dbReaderObject = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderObject.GetDataObjects(checkObjects);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading Objects";
                    return result;
                }

                if (!dbReaderObject.Objects1.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No References found";
                    return result;
                }

                (from checkObject in checkObjects.Where(obj => obj.New_Item == true)
                 join foundObject in dbReaderObject.Objects1 on checkObject.Name equals foundObject.Name
                 select foundObject).ToList().ForEach(obj =>
                 {
                     obj.New_Item = true;
                 });

                result.Result = dbReaderObject.Objects1;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<TimeTracking>>> GetTimetracking(List<TimeManagementModule.Models.TimeManagementEntry> timeManagementEntries)
        {
            var taskResult = await Task.Run<ResultItem<List<TimeTracking>>>(() =>
           {
               var result = new ResultItem<List<TimeTracking>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<TimeTracking>()
               };

               var searchTimeTrackings = timeManagementEntries.Select(time => new clsObjectRel
               {
                   ID_Other = time.IdTimeManagement,
                   ID_RelationType = Config.LocalData.ClassRel_Timetracking_belongs_to_Timemanagement.ID_RelationType,
                   ID_Parent_Object = Config.LocalData.ClassRel_Timetracking_belongs_to_Timemanagement.ID_Class_Left
               }).ToList();

               var dbReaderTimeTrackings = new OntologyModDBConnector(globals);

               if (searchTimeTrackings.Any())
               {
                   result.ResultState = dbReaderTimeTrackings.GetDataObjectRel(searchTimeTrackings);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Timetrackings!";
                       return result;
                   }
               }

               var searchTimeTrackingToIssue = dbReaderTimeTrackings.ObjectRels.Select(timeTrack => new clsObjectRel
               {
                   ID_Object = timeTrack.ID_Object,
                   ID_RelationType = Config.LocalData.ClassRel_Timetracking_belongs_to_Issue__Jira_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Timetracking_belongs_to_Issue__Jira_.ID_Class_Right
               }).ToList();


               var dbReaderTimeTrackingsToIssues = new OntologyModDBConnector(globals);

               if (searchTimeTrackingToIssue.Any())
               {
                   result.ResultState = dbReaderTimeTrackingsToIssues.GetDataObjectRel(searchTimeTrackingToIssue);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Timetrackings to Issues!";
                       return result;
                   }
               }

               result.Result = (from timeTrackingToTimemanagement in dbReaderTimeTrackings.ObjectRels
                                join timeTrackingToIssue in dbReaderTimeTrackingsToIssues.ObjectRels on timeTrackingToTimemanagement.ID_Object equals timeTrackingToIssue.ID_Object
                                select new TimeTracking
                                {
                                    IdTimeTracking = timeTrackingToTimemanagement.ID_Object,
                                    NameTimeTracking = timeTrackingToTimemanagement.Name_Object,
                                    IdTimeManagement = timeTrackingToTimemanagement.ID_Other,
                                    IdIssue = timeTrackingToIssue.ID_Other
                                }).ToList();


               
               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> GetObject(string idObject)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbReaderObject = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderObject.GetDataObjects(new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = idObject
                    }
                });

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the object!";
                    return result;
                }

                result.Result = dbReaderObject.Objects1.FirstOrDefault();

                if (result.Result == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the object!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveWrikeIssues(SaveWrikeIssueRequest request)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var relationConfig = new clsRelationConfig(globals);

                var saveTimeLogs = request.WrikeIssues.Where(entry => entry.TimeLogEntry != null);
                var issues = request.WrikeIssues.Select(wrikeIssue => wrikeIssue.Issue);
                var saveItems = issues.ToList();
                var sessionStamp = DateTime.Now;
                var workSession = new clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = sessionStamp.ToString("yyyy-MM-dd hh:mm:ss"),
                    GUID_Parent = Config.LocalData.Class_Sync_Session__Wrike_.GUID,
                    Type = globals.Type_Object
                };
                var sessionStampRel = relationConfig.Rel_ObjectAttribute(workSession, Config.LocalData.AttributeType_Datetimestamp__Create_, sessionStamp);
                saveItems.Add(workSession);
                saveItems.AddRange(request.WrikeIssues.Where(wrikeIssue => wrikeIssue.WrikeId != null).Select(wrikeIssue => wrikeIssue.WrikeId));
                saveItems.AddRange(request.WrikeIssues.Where(wrikeIssue => wrikeIssue.IssueUrl != null).Select(wrikeIssue => wrikeIssue.IssueUrl));
                saveItems.AddRange(saveTimeLogs.Select(timeLog => timeLog.TimeLogEntry.WrikeId));

                var dbWriterWrikeIssues = new OntologyModDBConnector(globals);

                if (saveItems.Any())
                {
                    result = dbWriterWrikeIssues.SaveObjects(saveItems);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving Issues and WrikeIds";
                        return result;
                    }
                }
                

                var wrikeIssues = request.WrikeIssues.Where(wrikeIssue => wrikeIssue.WrikeId != null).Select(wrikeIssue => wrikeIssue).ToList();
                var relations = wrikeIssues.Select(wrikeIssue => relationConfig.Rel_ObjectRelation(wrikeIssue.Issue, wrikeIssue.WrikeId, Config.LocalData.RelationType_is_defined_by)).ToList();
                relations.AddRange(wrikeIssues.Select(wrikeIssue => relationConfig.Rel_ObjectRelation(workSession, wrikeIssue.Issue, Config.LocalData.RelationType_contains)));
                relations.AddRange(request.WrikeIssues.Where(wrikeIssue => wrikeIssue.IssueUrl != null).Select(wrikeIssue => relationConfig.Rel_ObjectRelation(wrikeIssue.Issue, wrikeIssue.IssueUrl, Config.LocalData.RelationType_accessible_by)));
                relations.AddRange(issues.Select(issue => relationConfig.Rel_ObjectRelation(issue, request.Partner, Config.LocalData.RelationType_responsible_for)));
                relations.AddRange(saveTimeLogs.Select(timeLog => relationConfig.Rel_ObjectRelation(timeLog.TimeLogEntry.WrikeId, timeLog.TimeLogEntry.WrikeId, Config.LocalData.Object_OItem_belongs_to)));

                if (relations.Any())
                {
                    result = dbWriterWrikeIssues.SaveObjRel(relations);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving Issues to WrikeIds";
                        return result;
                    }
                }

                result = dbWriterWrikeIssues.SaveObjAtt(new List<clsObjectAtt> { sessionStampRel });

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving session-stamp!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }



        public ElasticAgent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}
