﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrikeConnectorModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.False,
        height = "100%")]
    [KendoFilter(mode = "row")]
    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoNumberFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", gt = "Greater than", gte = "Greater than or equal", lt = "Lower than", lte = "Lower than or equal")]
    [KendoDateFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", gt = "Greater than", gte = "Greater than or equal", lt = "Lower than", lte = "Lower than or equal")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class IssueGridItem : Issue__Jira_
    {
        //[KendoColumn(hidden = false, Order = 1, filterable = false, title = "Apply", template = "<input type=\"checkbox\" #= Apply ? 'checked=\"checked\"' : \"\" # 'class=\"chkbxApply\"' />", width = "80px")]
        [KendoColumn(hidden = false, Order = 1, filterable = false, title = "Apply", template = "<i #= Apply ? 'class=\"fa fa-check-square-o\"' : 'class=\"fa fa-square-o\"' # 'aria-hidden=\"true\"' />", width = "80px", type = ColType.BooleanType)]
        [KendoModel(editable = true)]
        public bool Apply { get; set; }

        [KendoColumn(hidden = true)]
        public string IdIssue
        {
            get
            {
                return Id;
            }
            set
            {
                Id = value;
            }
        }

        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Bezeichnung")]
        public string NameIssue
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        }

        [KendoColumn(hidden = true)]
        public string IdWrikeId
        {
            get
            {
                return Wrike_IdEntity?.Id;
            }
            set
            {
                if (Wrike_IdEntity == null)
                {
                    Wrike_IdEntity = new Wrike_Id();
                }
                Wrike_IdEntity.Id = value;
            }
        }

        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Task-Id")]
        public string NameWrikeId
        {
            get
            {
                return Wrike_IdEntity?.Name;
            }
            set
            {
                if (Wrike_IdEntity == null)
                {
                    Wrike_IdEntity = new Wrike_Id();
                }

                Wrike_IdEntity.Name = value;
            }
        }

        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Wrike-State")]
        public string WrikeState { get; set; }
        

        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Not in DB")]
        public bool IsNotInDb { get; set; }
        
        
    }
}
