﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taviloglu.Wrike.Core.Tasks;

namespace WrikeConnectorModule.Models
{
    public class SyncIssuesResult
    {
        public SyncIssuesRequest Request { get; set; }
        public List<IssueTask> Tasks { get; set; } = new List<IssueTask>();
    }

    public class IssueTask
    {
        public clsOntologyItem Issue { get; set; }
        public clsObjectRel IssueToTaskId { get; set; }
        public WrikeTask WrikeTask { get; set; }
    }
}
