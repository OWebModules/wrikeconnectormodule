﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrikeConnectorModule.Models
{
    public class SyncIssuesRequest : IGetModelSync
    {
        public string IdConfig { get; set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public SyncIssuesRequest(string idConfig)
        {
            IdConfig = idConfig;
        }

    }
}
