﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrikeConnectorModule.Models
{
    public class GetWrikeIssuesRequest
    {
        public string IdConfig { get; set; }

        public GetWrikeIssuesRequest(string idConfig)
        {
            IdConfig = idConfig;
        }

    }
}
