﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrikeConnectorModule.Models
{
    public class SaveWrikeIssueRequest
    {
        public clsOntologyItem Partner { get; set; }
        public List<WrikeIssue> WrikeIssues { get; set; } = new List<WrikeIssue>();
    }

    public class WrikeIssue
    {
        public clsOntologyItem Issue { get; set; }
        public clsOntologyItem WrikeId { get; set; }
        public clsOntologyItem IssueUrl { get; set; }

        public TimeLogEntry TimeLogEntry { get; set; }
        


    }

    public class TimeLogEntry
    {
        public clsOntologyItem TimemanagementEntry { get; set; }
        public clsOntologyItem WrikeId { get; set; }
    }
}
