﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrikeConnectorModule.Models
{
    public class GetModelResult
    {
        public clsOntologyItem Config { get; set; }
        public clsOntologyItem AuthenticationToken { get; set; }

        public clsObjectAtt AuthenticationTokenKey { get; set; }

        public clsOntologyItem Partner { get; set; }

        public clsOntologyItem UserItem { get; set; }

        public clsOntologyItem GroupItem { get; set; }

        public clsObjectAtt SyncTimeEntries { get; set; }

        public List<clsObjectRel> IssuesToPartner { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> IssuesToWrikeId { get; set; } = new List<clsObjectRel>();


        public List<clsObjectRel> IssueUrls { get; set; } = new List<clsObjectRel>();

        
    }
}
