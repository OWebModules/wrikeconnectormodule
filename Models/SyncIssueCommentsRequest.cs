﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrikeConnectorModule.Models
{
    public class SyncIssueCommentsRequest : IGetModelSync
    {
        public string IdConfig { get; set; }
        public List<IssueComment> IssueComments { get; set; }

        public SyncIssueCommentsRequest(string idConfig, List<IssueComment> issueComments)
        {
            IdConfig = idConfig;
            IssueComments = issueComments;
        }
    }
}
