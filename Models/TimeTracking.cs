﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrikeConnectorModule.Models
{
    public class TimeTracking
    {
        public string IdTimeManagement { get; set; }
        public string IdTimeTracking { get; set; }
        public string NameTimeTracking { get; set; }
        public string IdIssue { get; set; }
    }
}
