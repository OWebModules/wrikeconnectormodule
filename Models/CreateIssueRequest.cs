﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrikeConnectorModule.Models
{
    public class CreateIssueRequest
    {
        public List<string> IdIssueList { get; set; } = new List<string>();
        public List<string> NameIssueList { get; set; } = new List<string>();
    }
}
