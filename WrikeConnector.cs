﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using PartnerModule.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Taviloglu.Wrike.ApiClient;
using Taviloglu.Wrike.Core;
using Taviloglu.Wrike.Core.Timelogs;
using TimeManagementModule;
using WrikeConnectorModule.Models;
using WrikeConnectorModule.Services;

namespace WrikeConnectorModule
{
    

    public class WrikeConnector : AppController
    {
        public async Task<ResultItem<List<IssueGridItem>>> GetGridItems(SyncIssuesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<IssueGridItem>>>(async () =>
            {
                var result = new ResultItem<List<IssueGridItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<IssueGridItem>()
                };

                var elasticAgent = new ElasticAgent(Globals);

                var serviceResult = await elasticAgent.GetModel(request);

                result.ResultState = serviceResult.ResultState;

                var partnerController = new PartnerConnector(Globals);

                var partnerResult = await partnerController.GetPartnerData(serviceResult.Result.Partner);

                result.ResultState = partnerResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while loading Partner-Data";
                    return result;
                }

                try
                {
                    var client = new WrikeClient(serviceResult.Result.AuthenticationTokenKey.Val_String);


                    var contacts = await client.Contacts.GetAsync();

                    var contact = contacts.FirstOrDefault(cont => cont.FirstName == partnerResult.Result.PersonalData.NameVorname &&
                        cont.LastName == partnerResult.Result.PersonalData.NameNachname);

                    if (contact == null)
                    {
                        result.ResultState = Globals.LState_Nothing.Clone();
                        result.ResultState.Additional1 = "No Contact of Configuration found!";
                        return result;
                    }

                    var tasks = await client.Tasks.GetAsync(responsibles: new List<string> { contact.Id });


                    var tasksNotInDb = (from task in tasks
                                        join existingIssue in serviceResult.Result.IssuesToWrikeId on task.Id equals existingIssue.Name_Other into existingIssues
                                        from existingIssue in existingIssues.DefaultIfEmpty()
                                        where existingIssue == null
                                        select new IssueGridItem
                                        {
                                            NameIssue = task.Title,
                                            NameWrikeId = task.Id,
                                            IsNotInDb = true
                                        });

                    result.Result = (from issue in serviceResult.Result.IssuesToWrikeId
                                     join task in tasks on issue.Name_Other equals task.Id into tasks1
                                     from task in tasks1.DefaultIfEmpty()
                                     select new IssueGridItem
                                     {
                                         IdIssue = issue.ID_Object,
                                         NameIssue = issue.Name_Object,
                                         IdWrikeId = issue.ID_Other,
                                         NameWrikeId = issue.Name_Other,
                                         WrikeState = task?.Status.ToString()
                                     }).ToList();

                    result.Result.AddRange(tasksNotInDb);
                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Error while connecting to Wrike: {ex.Message}";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<SyncIssueCommentsResult>> SyncSourceCodeComments(SyncIssueCommentsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SyncIssueCommentsResult>>(async () =>
           {
               var result = new ResultItem<SyncIssueCommentsResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new SyncIssueCommentsResult()
               };

               var elasticAgent = new ElasticAgent(Globals);

               var serviceResult = await elasticAgent.GetModel(request);

               result.ResultState = serviceResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               var partnerController = new PartnerConnector(Globals);

               var partnerResult = await partnerController.GetPartnerData(serviceResult.Result.Partner);

               result.ResultState = partnerResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while loading Partner-Data";
                   return result;
               }

               if (partnerResult.Result.PersonalData == null ||
                   string.IsNullOrEmpty(partnerResult.Result.PersonalData.NameNachname) ||
                   string.IsNullOrEmpty(partnerResult.Result.PersonalData.NameVorname))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Error while loading Partner-Data";
                   return result;
               }

               try
               {
                   var relationConfig = new clsRelationConfig(Globals);
                   var client = new WrikeClient(serviceResult.Result.AuthenticationTokenKey.Val_String);


                   var contacts = await client.Contacts.GetAsync();

                   var contact = contacts.FirstOrDefault(cont => cont.FirstName == partnerResult.Result.PersonalData.NameVorname &&
                       cont.LastName == partnerResult.Result.PersonalData.NameNachname);

                   if (contact == null)
                   {
                       result.ResultState = Globals.LState_Nothing.Clone();
                       result.ResultState.Additional1 = "No Contact of Configuration found!";
                       return result;
                   }


                   var tasks = await client.Tasks.GetAsync(responsibles: new List<string> { contact.Id });

                   var issuesResult = await elasticAgent.GetIssueRelations(tasks);

                   var typedTaggingController = new TypedTaggingModule.Connectors.TypedTaggingConnector(Globals);
                   foreach (var issueComment in request.IssueComments)
                   {
                       var comment = $"{issueComment.CreateDate} - Git Commit {issueComment.Commit.Name} - {issueComment.Comment}";
                       var regexComment = new Regex( $"(Commit|Changeset) {issueComment.Commit.Name}");
                       
                       var task = tasks.FirstOrDefault(tsk => issueComment.Comment.Contains($"#{tsk.Id}"));

                       if (task != null)
                       {
                           var issueRel = issuesResult.Result.FirstOrDefault(rel => rel.Name_Other == task.Id);
                           if (issueRel == null)
                           {
                               result.ResultState = Globals.LState_Error.Clone();
                               result.ResultState.Additional1 = $"The issue of task {task.Id} cannot be found!";
                               return result;
                           }
                           try
                           {
                               var comments = await client.Comments.GetInTaskAsync(task.Id);
                               if (!comments.Any(com => regexComment.Match(com.Text).Success))
                               {
                                   var commentInWrike = new Taviloglu.Wrike.Core.Comments.WrikeTaskComment(comment, task.Id)
                                   {
                                       CreatedDate = issueComment.CreateDate
                                   };

                                   var wrikeComment = await client.Comments.CreateAsync(commentInWrike, true);

                                   var typedTagResult = await typedTaggingController.SaveTags(issueComment.Commit, new List<clsOntologyItem>
                                   {
                                       new clsOntologyItem
                                       {
                                           GUID = issueRel.ID_Object,
                                           Name = issueRel.Name_Object,
                                           GUID_Parent = issueRel.ID_Parent_Object,
                                           Type = Globals.Type_Object
                                       }
                                   },
                                   serviceResult.Result.UserItem,
                                   serviceResult.Result.GroupItem, false, null);

                                   result.ResultState = typedTagResult.Result;

                                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                   {
                                       return result;
                                   }

                               }
                           }
                           catch (Exception)
                           {

                               
                           }
                           
                       }
                   }


               }
               catch (Exception ex)
               {

                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"Error while connecting to Wrike: {ex.Message}";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<SyncIssuesResult>> SyncTasks(SyncIssuesRequest request)
        {
            var result = new ResultItem<SyncIssuesResult>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new SyncIssuesResult()
            };

            var elasticAgent = new ElasticAgent(Globals);

            request.MessageOutput?.OutputInfo("Get Model...");
            var serviceResult = await elasticAgent.GetModel(request);
            

            result.ResultState = serviceResult.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                return result;
            }
            request.MessageOutput?.OutputInfo("Have Model.");

            var partnerController = new PartnerConnector(Globals);

            request.MessageOutput?.OutputInfo("Get Partner...");
            var partnerResult = await partnerController.GetPartnerData(serviceResult.Result.Partner);

            result.ResultState = partnerResult.ResultState;
            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while loading Partner-Data";
                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                return result;
            }

            if (partnerResult.Result.PersonalData == null ||
                string.IsNullOrEmpty(partnerResult.Result.PersonalData.NameNachname) ||
                string.IsNullOrEmpty(partnerResult.Result.PersonalData.NameVorname))
            {
                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = "Error while loading Partner-Data";
                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                return result;
            }

            request.MessageOutput?.OutputInfo("Have Partner...");

            var timeManagementController = new TimeManagementController(Globals);

            request.MessageOutput?.OutputInfo("Get Timeentries...");
            var timeManagementEntriesResult = await timeManagementController.GetTimeManagementEntryList(serviceResult.Result.UserItem, serviceResult.Result.GroupItem);

            result.ResultState = timeManagementEntriesResult.ResultState;
            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while getting the TimemanagementEntries!";
                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                return result;
            }

            request.MessageOutput?.OutputInfo($"Count Timeentries: {timeManagementEntriesResult.Result.EntryList.Count}");

            var timeManagementEntries = timeManagementEntriesResult.Result.EntryList;

            request.MessageOutput?.OutputInfo($"Count Timetrackings");
            var timeTrackingsResult = await elasticAgent.GetTimetracking(timeManagementEntries);

            result.ResultState = timeTrackingsResult.ResultState;
            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                return result;
            }

            var timeTrackings = (from timeManagementEntry in timeManagementEntries
                                 join timeTracking in timeTrackingsResult.Result on timeManagementEntry.IdTimeManagement equals timeTracking.IdTimeManagement into timeTrackings1
                                 from timeTracking in timeTrackings1.DefaultIfEmpty()
                                 select new { timeManagementEntry, timeTracking });

            request.MessageOutput?.OutputInfo($"Count Timetrackings: {timeTrackings.Count()}");
            var taskResult = await Task.Run<ResultItem<SyncIssuesResult>>(async () =>
            {
                result = new ResultItem<SyncIssuesResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new SyncIssuesResult()
                };

                try
                {
                    var relationConfig = new clsRelationConfig(Globals);
                    var transaction = new clsTransaction(Globals);
                    var client = new WrikeClient(serviceResult.Result.AuthenticationTokenKey.Val_String);

                    request.MessageOutput?.OutputInfo($"Get Contact");
                    var contacts = await client.Contacts.GetAsync();

                    var contact = contacts.FirstOrDefault(cont => cont.FirstName == partnerResult.Result.PersonalData.NameVorname &&
                        cont.LastName == partnerResult.Result.PersonalData.NameNachname);

                    if (contact == null)
                    {
                        result.ResultState = Globals.LState_Nothing.Clone();
                        result.ResultState.Additional1 = "No Contact of Configuration found!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    request.MessageOutput?.OutputInfo($"Have Contact");

                    request.MessageOutput?.OutputInfo($"Get Tasks");
                    var tasks = await client.Tasks.GetAsync(responsibles: new List<string> { contact.Id });

                    request.MessageOutput?.OutputInfo($"Count Tasks: {tasks.Count}");

                    var wrikeIssues = (from issue in serviceResult.Result.IssuesToPartner
                                       join wrikeId in serviceResult.Result.IssuesToWrikeId on issue.ID_Object equals wrikeId.ID_Object
                                       join issueUrl in serviceResult.Result.IssueUrls on issue.ID_Object equals issueUrl.ID_Object
                                       select new { issue, wrikeId, issueUrl });

                    request.MessageOutput?.OutputInfo($"Count WrikeIssues: {wrikeIssues.Count()}");

                    var tasksAndIssues = (from task in tasks
                                          join issue in wrikeIssues on task.Id equals issue.wrikeId.Name_Other into issues
                                          from issue in issues.DefaultIfEmpty()
                                          select new { task, issue });

                    request.MessageOutput?.OutputInfo($"Count TasksAndIssues: {tasksAndIssues.Count()}");

                    var saveRequest = new SaveWrikeIssueRequest
                    {
                        Partner = serviceResult.Result.Partner
                    };

                    foreach (var taskIssue in tasksAndIssues)
                    {
                        request.MessageOutput?.OutputInfo($"TaskIssue: {taskIssue.task.Id}...");

                        if (taskIssue.issue == null)
                        {
                            request.MessageOutput?.OutputInfo($"Issue is existing.");
                            var issue = new clsOntologyItem
                            {
                                GUID = Globals.NewGUID,
                                Name = taskIssue.task.Title,
                                GUID_Parent = Config.LocalData.Class_Issue__Jira_.GUID,
                                Type = Globals.Type_Object
                            };

                            var wrikeId = new clsOntologyItem
                            {
                                GUID = Globals.NewGUID,
                                Name = taskIssue.task.Id,
                                GUID_Parent = Config.LocalData.Class_Wrike_Id.GUID,
                                Type = Globals.Type_Object
                            };

                            var issueUrl = new clsOntologyItem
                            {
                                GUID = Globals.NewGUID,
                                Name = taskIssue.task.Permalink,
                                GUID_Parent = Config.LocalData.Class_Url.GUID,
                                Type = Globals.Type_Object
                            };

                            saveRequest.WrikeIssues.Add(new WrikeIssue
                            {
                                Issue = issue,
                                WrikeId = wrikeId,
                                IssueUrl = issueUrl
                            });

                        }
                        else
                        {
                            request.MessageOutput?.OutputInfo($"Issue is not existing.");
                            if (taskIssue.issue.issue.Name_Object != taskIssue.task.Title)
                            {
                                saveRequest.WrikeIssues.Add(new WrikeIssue
                                {
                                    Issue = new clsOntologyItem
                                    {
                                        GUID = taskIssue.issue.issue.ID_Object,
                                        Name = taskIssue.task.Title,
                                        GUID_Parent = Config.LocalData.Class_Issue__Jira_.GUID,
                                        Type = Globals.Type_Object
                                    }
                                });
                            }

                            if (serviceResult.Result.SyncTimeEntries != null && serviceResult.Result.SyncTimeEntries.Val_Bit.Value)
                            {
                                request.MessageOutput?.OutputInfo($"Sync Timentries.");
                                var times = await client.Timelogs.GetAsync(taskId: taskIssue.task.Id);
                                var trackings = timeTrackings.Where(tracking => tracking.timeManagementEntry.IdReference == taskIssue.issue.issue.ID_Object).ToList();
                                trackings.AddRange(timeTrackings.Where(tracking => tracking.timeManagementEntry.IdReference == taskIssue.issue.wrikeId.ID_Other));

                                var trackingsToWrike = trackings.Where(track => track.timeTracking == null);

                                request.MessageOutput?.OutputInfo($"Save Timentries: {trackingsToWrike.Count()}");
                                foreach (var tracking in trackingsToWrike)
                                {
                                    var timeLog = new WrikeTimelog(taskIssue.task.Id, (decimal)tracking.timeManagementEntry.Ende.Subtract(tracking.timeManagementEntry.Start).TotalHours, tracking.timeManagementEntry.Start, comment: tracking.timeManagementEntry.NameTimeManagement);
                                    timeLog.UserId = contact.Id;
                                    try
                                    {
                                        var logEntry = await client.Timelogs.CreateAsync(timeLog, true);
                                        var timeLogToSave = new clsOntologyItem
                                        {
                                            GUID = Globals.NewGUID,
                                            Name = logEntry.Id,
                                            GUID_Parent = Config.LocalData.Class_Timetracking.GUID,
                                            Type = Globals.Type_Object
                                        };

                                        transaction.ClearItems();
                                        result.ResultState = transaction.do_Transaction(timeLogToSave);

                                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                        {
                                            await client.Timelogs.DeleteAsync(logEntry.Id);
                                            result.ResultState.Additional1 = "Error while saving the Timetracking!";
                                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                            return result;
                                        }

                                        var rel = relationConfig.Rel_ObjectRelation(timeLogToSave, new clsOntologyItem
                                        {
                                            GUID = taskIssue.issue.issue.ID_Object,
                                            Name = taskIssue.issue.issue.Name_Object,
                                            GUID_Parent = taskIssue.issue.issue.ID_Parent_Object,
                                            Type = Globals.Type_Object
                                        }, Config.LocalData.RelationType_belongs_to);

                                        result.ResultState = transaction.do_Transaction(rel);

                                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                        {
                                            await client.Timelogs.DeleteAsync(logEntry.Id);
                                            transaction.rollback();
                                            result.ResultState.Additional1 = "Error while saving the Timetracking!";
                                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                            return result;
                                        }

                                        var rel1 = relationConfig.Rel_ObjectRelation(timeLogToSave, new clsOntologyItem
                                        {
                                            GUID = tracking.timeManagementEntry.IdTimeManagement,
                                            Name = tracking.timeManagementEntry.NameTimeManagement,
                                            GUID_Parent = timeManagementController.ClassTimeManagement.GUID,
                                            Type = Globals.Type_Object
                                        }, Config.LocalData.RelationType_belongs_to);

                                        result.ResultState = transaction.do_Transaction(rel1);

                                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                        {
                                            await client.Timelogs.DeleteAsync(logEntry.Id);
                                            transaction.rollback();
                                            result.ResultState.Additional1 = "Error while saving the Timetracking!";
                                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                            return result;
                                        }
                                    }
                                    catch (Exception)
                                    {

                                    }

                                }
                            }
                            
                        }




                    }

                    
                    if (saveRequest.WrikeIssues.Any())
                    {
                        request.MessageOutput?.OutputInfo($"Issues to save: {saveRequest.WrikeIssues.Count}");
                        result.ResultState = await elasticAgent.SaveWrikeIssues(saveRequest);
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        request.MessageOutput?.OutputInfo($"Issues saved");
                    }
                    else
                    {
                        request.MessageOutput?.OutputInfo($"No Issues to save!");
                    }
                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Error while connecting to Wrike: {ex.Message}";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultOItem<ClassObject>> GetClassObjectReference(clsOntologyItem refItem)
        {
            var serviceElastic = new ElasticAgent(Globals);

            var reference = await serviceElastic.GetClassObject(refItem);

            if (reference.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return reference;
            }

            if (reference.Result.ObjectItem.GUID_Parent != Config.LocalData.Class_WrikeConnectorModule.GUID)
            {
                reference.ResultState = Globals.LState_Nothing.Clone();
                reference.ResultState.Additional1 = "The Reference is no Wrike Connector Config";
            }

            return reference;
        }


        public async Task<ResultItem<CreateIssueResult>> CreateWrikeIssue(CreateIssueRequest request)
        {
            var result = new ResultItem<CreateIssueResult>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new CreateIssueResult()
            };
            if ((request.IdIssueList == null && request.NameIssueList == null) || (!request.IdIssueList.Any() && !request.NameIssueList.Any()))
            {
                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = "You must provide at least one id of an existing or at least one name of a new Issue";
                return result;
            }

            if (request.IdIssueList.Any())
            {
                if (request.IdIssueList.Any(id => string.IsNullOrEmpty(id) || !Globals.is_GUID(id)))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "One provided Id is not valid";
                    return result;
                }
            }


            if (request.NameIssueList.Any())
            {
                if (request.NameIssueList.Any(name => string.IsNullOrEmpty(name)))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "One provided name is not valid";
                    return result;
                }
            }

            if (request.IdIssueList == null || request.NameIssueList == null)
            {
                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = "The Lists are invalid (null)";
                return result;
            }



            var taskResult = await Task.Run<ResultItem<CreateIssueResult>>(async () =>
            {
                var resultAsync = new ResultItem<CreateIssueResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new CreateIssueResult()
                };

                var issues = request.IdIssueList.Select(id => new clsOntologyItem
                {
                    GUID = id,
                    GUID_Parent = Config.LocalData.Class_Issue__Jira_.GUID,
                    Type = Globals.Type_Object,
                    New_Item = false
                }).ToList();

                issues.AddRange(request.NameIssueList.Select(name => new clsOntologyItem
                {
                    Name = name,
                    GUID_Parent = Config.LocalData.Class_Issue__Jira_.GUID,
                    Type = Globals.Type_Object,
                    New_Item = true
                }));


                var elasticAgent = new Services.ElasticAgent(Globals);

                var oItemResult = await elasticAgent.CheckObjects(issues);
                resultAsync.ResultState = oItemResult.ResultState;

                if (resultAsync.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the Issue-Items";
                    return result;
                }





                return resultAsync;
            });


            return taskResult;
        }

        public WrikeConnector(Globals globals) : base(globals)
        {
        }
    }
}
